package main

import (
	"errors"
	"fmt"
	"github.com/fatih/color"
	"gitlab.com/MikeTTh/env"
	"gitlab.com/disappointment-industries/arch-auto-mirror/mirror"
	"gitlab.com/disappointment-industries/arch-auto-mirror/synchronize"
	"gitlab.com/disappointment-industries/arch-auto-mirror/web"
	"math/rand"
	"net/http"
	"os"
	"sync"
	"time"
)

func init() {
	_ = os.MkdirAll("./files", os.ModePerm)
}

const mirrorEnv = "MIRROR"

var doHttp = env.Bool("HTTP", true)
var doSync = env.Bool("SYNC", true)
var doCron = env.Bool("CRON", true)

func retry(f func() error) error {
	i := 0
	var e = errors.New("")
	for i < 3 && e != nil {
		e = f()
		i++
		if e != nil && i < 3 {
			color.Red("try: %d", i)
			color.Red("error: %s", e)
			time.Sleep(5 * time.Second)
		}
	}
	return e
}

const dateFmt = "Jan 2 15:04:05 -0700 MST"

func main() {
	var wg sync.WaitGroup

	if doSync {
		wg.Add(1)

		go func() {
			var mirr string

			if env.Exists(mirrorEnv) {
				mirr = env.String(mirrorEnv, "")
				color.Blue("=> chosen mirror %s", mirr)
			} else {
				color.Blue("=> getting fastest mirror")
				e := retry(func() error {
					var e error
					mirr, e = mirror.GetFastestMirror()
					return e
				})
				if e != nil {
					panic(e)
				}

				color.Green("-> found fastest mirror: %s", mirr)
			}

			syncr := func() {
				fmt.Println()
				color.Blue("=> repo sync started at %s", time.Now().Format(dateFmt))
				e := retry(func() error {
					return synchronize.Sync(mirr)
				})
				if e != nil {
					panic(e)
				}
				color.Green("=> repo sync ended at %s", time.Now().Format(dateFmt))
				fmt.Println()
			}

			if !doCron {
				syncr()
			}

			if doCron {
				wg.Add(1)
				rand.Seed(time.Now().UnixNano())
				go func() {
					for {
						syncr()
						r := rand.Intn(60)
						nextTime := time.Now().Truncate(time.Hour).Add(time.Hour).Add(time.Minute * time.Duration(r))
						color.Blue("-> next sync will be at %s", nextTime.Format(dateFmt))
						time.Sleep(time.Until(nextTime))
					}
				}()
			}

			wg.Done()
		}()
	}

	if doHttp {
		wg.Add(1)

		go func() {
			color.Blue("=> serving on %s", env.String("HTTPADDR", ":8080"))
			e := http.ListenAndServe(env.String("HTTPADDR", ":8080"), web.FileServer("files"))
			if e != nil {
				panic(e)
			}
			wg.Done()
		}()
	}

	wg.Wait()
}
