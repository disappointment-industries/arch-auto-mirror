package rsync

import (
	"fmt"
	"os"
	"os/exec"
	"time"
)

var Opts = []string{
	"-rtlvH",
	"--delete-after",
	"--delay-updates",
	"--safe-links",
}

func Rsync(from, to string, verbose bool, extraflags []string, timeout time.Duration) error {
	var running = true
	defer func() { running = false }()

	opts := Opts
	if extraflags != nil {
		opts = append(opts, extraflags...)
	}
	opts = append(opts, from, to)
	fmt.Print("running command: rsync ")
	for _, o := range opts {
		fmt.Print(o, " ")
	}
	if timeout != 0 {
		fmt.Printf("(%s timeout)", timeout.String())
	}
	fmt.Println()
	cmd := exec.Command("rsync", opts...)
	if verbose {
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
	}
	if timeout != 0 {
		go func() {
			time.Sleep(timeout)
			if running {
				_ = cmd.Process.Kill()
			}
		}()
	}
	return cmd.Run()
}
