package synchronize

import (
	"gitlab.com/MikeTTh/env"
	"gitlab.com/disappointment-industries/arch-auto-mirror/rsync"
	"strings"
)

var toSync = strings.Split(env.String("TOSYNC", "pool,core,community,extra,testing,gnome-unstable,kde-unstable,multilib,iso,images"), ",")
var toIgnore = strings.Split(env.String("TOIGNORE", "*-debug"), ",")

func Sync(url string) error {
	opts := make([]string, 0, len(toIgnore))
	for _, s := range toIgnore {
		opts = append(opts, "--exclude", s)
	}

	for _, s := range toSync {
		e := rsync.Rsync(url+s, "files/", true, opts, 0)
		if e != nil {
			return e
		}
	}
	return nil
}
