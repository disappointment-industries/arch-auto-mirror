FROM archlinux:latest

WORKDIR /app

RUN pacman --noconfirm -Syu rsync

ADD app /app/

VOLUME /app/files

ENTRYPOINT ["/app/app"]