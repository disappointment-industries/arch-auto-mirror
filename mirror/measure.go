package mirror

import (
	"gitlab.com/MikeTTh/env"
	"gitlab.com/disappointment-industries/arch-auto-mirror/rsync"
	"os"
	"time"
)

const dest = "/tmp/measure"

var timeout = time.Duration(env.Int("MEASURE_TIMEOUT", 10000)) * time.Millisecond

func measure(url string) (uint, error) {
	_ = os.RemoveAll(dest)
	e := os.MkdirAll("/tmp/measure", os.ModePerm)
	if e != nil {
		return 0, e
	}

	start := time.Now()

	e = rsync.Rsync(url+"pool/packages/linux-lts-*.pkg.*", dest+"/", false, nil, timeout)
	if e != nil {
		return 0, e
	}

	end := time.Now()
	dur := end.Sub(start)

	_ = os.RemoveAll(dest)

	return uint(dur.Milliseconds()), nil
}
