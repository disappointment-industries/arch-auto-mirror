package mirror

import (
	"fmt"
	"math"
	"sort"
)

func GetFastestMirror() (string, error) {
	mirrors, e := getTier2Mirrors()
	if e != nil {
		return "", e
	}

	// Only keep top 10 mirrors based on score
	x := 10
	if len(mirrors) < x {
		x = len(mirrors)
	}

	mirrors = mirrors[:x-1]

	times := make(map[string]uint)

	for _, m := range mirrors {
		times[m], e = measure(m)
		if e != nil {
			times[m] = math.MaxUint64
		}
		fmt.Printf("%s:\t%d ms\n", m, times[m])
	}

	sort.Slice(mirrors, func(i, j int) bool {
		return times[mirrors[i]] < times[mirrors[j]]
	})

	return mirrors[0], nil
}
