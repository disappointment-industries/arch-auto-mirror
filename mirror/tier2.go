package mirror

import (
	"encoding/json"
	"gitlab.com/MikeTTh/env"
	"io/ioutil"
	"net/http"
	"sort"
)

var (
	needISO  = env.Bool("ISO", true)
	needIPv4 = env.Bool("IPv4", true)
	needIPv6 = env.Bool("IPv6", false)
)

type api struct {
	Cutoff int
	Urls   []struct {
		Url           string
		Protocol      string
		CompletionPct float64 `json:"completion_pct"`
		Score         *float64
		Active        bool
		ISOs          bool
		IPv4          bool
		IPv6          bool
	}
}

func (a *api) Sort() {
	sort.Slice(a.Urls, func(i, j int) bool {
		if a.Urls[i].Score == nil {
			return false
		}
		if a.Urls[j].Score == nil {
			return true
		}
		return *a.Urls[i].Score < *a.Urls[j].Score
	})
}

func getTier2Mirrors() ([]string, error) {
	r, e := http.Get("https://archlinux.org/mirrors/status/tier/1/json/")
	if e != nil {
		return nil, e
	}

	bod, e := ioutil.ReadAll(r.Body)
	if e != nil {
		return nil, e
	}

	var resp api
	e = json.Unmarshal(bod, &resp)
	if e != nil {
		return nil, e
	}

	resp.Sort()

	dun := make([]string, 0)

	for _, m := range resp.Urls {
		if m.Protocol == "rsync" && (!needIPv4 || m.IPv4) && (!needIPv6 || m.IPv6) && (!needISO || m.ISOs) {
			dun = append(dun, m.Url)
		}
	}

	return dun, nil
}
