module gitlab.com/disappointment-industries/arch-auto-mirror

replace gitlab.com/disappointment-industries/arch-auto-mirror => ./

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.8.0 // indirect
	github.com/fatih/color v1.10.0
	github.com/m3ng9i/go-utils v0.0.0-20160811013010-f9b7dc669fde // indirect
	github.com/m3ng9i/ran v0.1.6 // indirect
	github.com/miekg/dns v1.1.49 // indirect
	gitlab.com/MikeTTh/env v0.0.0-20201224200853-8b987dda2f63
	golang.org/x/crypto v0.0.0-20220525230936-793ad666bf5e // indirect
	golang.org/x/net v0.0.0-20220607020251-c690dde0001d // indirect
)
