package web

import (
	_ "embed"
	"github.com/m3ng9i/go-utils/log"
	"github.com/m3ng9i/ran/server"
	"gitlab.com/MikeTTh/env"
	"io/ioutil"
	"net/http"
	"os"
)

//go:embed favicon.ico
var icon []byte

var logger *log.Logger

var shouldLog = env.Bool("LOG", true)

func init() {
	var config log.Config
	config.Layout = log.LY_DEFAULT
	config.LayoutStyle = log.LS_DEFAULT
	config.TimeFormat = log.TF_DEFAULT
	config.Level = log.WARN

	out := ioutil.Discard
	if shouldLog {
		out = os.Stdout
	}

	var err error
	logger, err = log.New(out, config)
	if err != nil {
		panic(err)
	}
}

func FileServer(path string) http.Handler {
	var ran = server.NewRanServer(server.Config{
		ListDir:  true,
		Root:     path,
		NoCache:  false,
		Gzip:     false,
		ServeAll: true,
	}, logger)

	fileServer := ran.Serve()
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Cache-Control", "public, max-age=604800, immutable")
		if r.URL.Path == "/favicon.ico" {
			w.Header().Add("Content-Type", "image/x-icon")
			_, _ = w.Write(icon)
			return
		}
		fileServer(w, r)
	})
}
